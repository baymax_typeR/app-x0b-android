package wikrama.isyana.appx0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_mahasiswa.*

class AdapterDataMhs(val dataMhs : List<HashMap<String, String>>,
                     val mahasiswaActivity: MahasiswaActivity) : //new
    RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMhs.HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mhs,p0,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(p0: AdapterDataMhs.HolderDataMhs, p1: Int) {
        val data = dataMhs.get(p1)
        p0.txNim.setText(data.get("nim"))
        p0.txNama.setText(data.get("nama"))
        p0.txProdi.setText(data.get("nama_prodi"))

        //beginNew
            if (p1.rem(2) == 0 ) p0.cLayout.setBackgroundColor(
                Color.rgb(230,246,240))
            else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

            p0.cLayout.setOnClickListener(View.OnClickListener {
                val pos = mahasiswaActivity.daftarProdi.indexOf(data.get("nama_prodi"))
                mahasiswaActivity.spProdi.setSelection(pos)
                mahasiswaActivity.edNim.setText(data.get("nim"))
                mahasiswaActivity.edNamaMhs.setText(data.get("nama"))
                Picasso.get().load(data.get("uri")).into(mahasiswaActivity.imgUpload);
            })

        //endNew

        if(!data.get("url").equals(""))
           Picasso.get().load(data.get("url")).into(p0.photo);
        p0.txTglLhr.setText(data.get("tgllahir"))
        p0.txGender.setText(data.get("gender"))
        p0.txAlamat.setText(data.get("alamat"))
    }

    class HolderDataMhs(v : View) : RecyclerView.ViewHolder(v) {
        val txNim = v.findViewById<TextView>(R.id.txNim)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txProdi = v.findViewById<TextView>(R.id.txProdi)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val txTglLhr = v.findViewById<TextView>(R.id.txTglLhr)
        val txGender = v.findViewById<TextView>(R.id.txGender)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.clayout) //new
    }
}