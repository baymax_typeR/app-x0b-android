package wikrama.isyana.appx0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_mahasiswa.*
import kotlinx.android.synthetic.main.row_mhs.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class MahasiswaActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload->{
                // val intent  = Intent() =======================
                //intent.setType("image/*") ========================================   ambil dari galery
                //intent.setAction(Intent.ACTION_GET_CONTENT)=====================
                // startActivityForResult(intent,MediaHelper.getRcGalery()) =======
                requestPermissions()
            }
            R.id.btnInsert->{
                query("insert")

            }
            R.id.btnDelete->{
                query("delete")

            }
            R.id.btnUpdate->{
                query("update")

            }
            R.id.btnFind->{

            }

        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var mhsAdapter: AdapterDataMhs
    lateinit var prodiAdapter: ArrayAdapter<String>
    var daftarMhs = mutableListOf<HashMap<String, String>>()
    var daftarProdi = mutableListOf<String>()
    val uri = "http://192.168.43.39/app-x0b-web/show_data.php"
    val uri1 = "http://192.168.43.39/app-x0b-web/show_prodi.php"
    val uri3 = "http://192.168.43.39/app-x0b-web/query_data.php"
    val url4 = ""
    var imStr = ""
    var namafile = ""
    var pilihProdi = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mahasiswa)
        mhsAdapter = AdapterDataMhs(daftarMhs, this) //new
        mediaHelper = MediaHelper(this)
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter
        prodiAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarProdi)
        spProdi.adapter = prodiAdapter
        spProdi.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        aka()
        getNamaProdi()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spProdi.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }
    }
    fun aka(){
        val request = StringRequest(Request.Method.POST,uri,Response.Listener { response ->
            daftarMhs.clear()
            val jsonArray = JSONArray(response)

            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var mhs = HashMap<String,String>()
                mhs.put("nim",jsonObject.getString("nim"))
                mhs.put("nama",jsonObject.getString("nama"))
                mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                mhs.put("url",jsonObject.getString("url"))
                daftarMhs.add(mhs)
            }
            mhsAdapter.notifyDataSetChanged()

        },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun uploadFile() {
        val request = object : StringRequest(Method.POST, url4, Response.Listener { response -> },
            Response.ErrorListener { error -> }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("imstr", imStr)
                hm.put("namaFile", namafile)
                return hm
            }
        }
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    fun requestPermissions() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    ) {
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imgUpload)
            } else if (requestCode == mediaHelper.getRcCamera()) {
                imStr = mediaHelper.getBitmapToString(fileUri, imageView)
                namafile = mediaHelper.getMyFileName()
            }
        }
    }

    fun getNamaProdi() {
        val request = StringRequest(Request.Method.POST, uri1,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error -> }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun query(mode: String) {
        val request = object : StringRequest(Method.POST, uri3,
            Response.Listener { response ->
                val jsonobject = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    aka()
                } else {
                    Toast.makeText(this, "Operasi Gagal ", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                //var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"  = > memberinama pada foto
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("nim", edNim.text.toString())
                        hm.put("nama", edNamaMhs.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", namafile)
                        hm.put("nama_prodi", pilihProdi)
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("nim", edNim.text.toString())
                        hm.put("nama", edNamaMhs.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", namafile)
                        hm.put("nama_prodi", pilihProdi)
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("nim", edNim.text.toString())
                    }
                }
                return hm
            }
        }
    }
    }


