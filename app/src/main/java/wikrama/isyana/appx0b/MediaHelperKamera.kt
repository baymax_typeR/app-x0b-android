package wikrama.isyana.appx0b

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.widget.ImageView
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.log

 class MediaHelperKamera {

    var namafile = ""
    var fileUri = Uri.parse("")
    val RC_camera = 100


    fun getMyfile(): String{
        return this.namafile
    }

    fun getRcCamera() : Int{
        return this.RC_camera
    }

    fun getOutputMediahelper() : File? {
        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory
            (Environment.DIRECTORY_DCIM),"DimasSimpan")
        if(!mediaStorageDir.exists())
            if (!mediaStorageDir.mkdirs()){
                Log.e("mkdir","Gagal Membuat direktori")
            }
        val mediafile = File(mediaStorageDir.path+File.separator+"${this.namafile}")
        return  mediafile
    }

    fun getOutputFileUri() : Uri {
        //var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"  = > memberinama pada foto
        //"DC+{$timestamp}.jpg"
       // val timestamp = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())
        this.namafile =   "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
        this.fileUri = Uri.fromFile(getOutputMediahelper())
        return this.fileUri
    }

    fun bitmaptoString(bpm : Bitmap) : String{
        val OutputStream = ByteArrayOutputStream()
        bpm.compress(Bitmap.CompressFormat.JPEG,60,OutputStream)
        val byteArray= OutputStream.toByteArray()
        return Base64.encodeToString(byteArray,Base64.DEFAULT)
    }


    fun getBitmapToString(imv : ImageView, uri : Uri) : String {
        var bpm = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888)
        bpm = BitmapFactory.decodeFile(this.fileUri.path)

        var dim = 720
        if (bpm.height > bpm.width){
            bpm = Bitmap.createScaledBitmap(bpm,
                (bpm.width*dim).div(bpm.height),dim,true)
        }else{
            bpm = Bitmap.createScaledBitmap(bpm,
                dim,(bpm.height*dim).div(bpm.width),true)
        }

        imv.setImageBitmap(bpm)
        return bitmaptoString(bpm)
    }


 }